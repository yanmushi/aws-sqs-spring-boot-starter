package com.yanmushi.aws.sqs;

/**
 * @author yinlei
 * @since 2021/1/27
 */
public class IllegalMessageException extends Exception {

    public IllegalMessageException() {
    }

    public IllegalMessageException(String message) {
        super(message);
    }

    public IllegalMessageException(String message, Throwable cause) {
        super(message, cause);
    }

    public IllegalMessageException(Throwable cause) {
        super(cause);
    }

    public IllegalMessageException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
