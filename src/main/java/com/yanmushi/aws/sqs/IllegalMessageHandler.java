package com.yanmushi.aws.sqs;

import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.model.Message;

/**
 * @author yinlei
 * @since 2021/1/27
 */
public interface IllegalMessageHandler {

    void handle(AmazonSQS amazonSQS, String queue, Message message);
}
