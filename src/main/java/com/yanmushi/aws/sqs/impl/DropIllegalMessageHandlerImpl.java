package com.yanmushi.aws.sqs.impl;

import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.model.Message;
import com.yanmushi.aws.sqs.IllegalMessageHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author yinlei
 * @since 2021/1/27
 */
public class DropIllegalMessageHandlerImpl implements IllegalMessageHandler {

    private static final Logger log = LoggerFactory.getLogger(DropIllegalMessageHandlerImpl.class);

    @Override
    public void handle(AmazonSQS amazonSQS, String queue, Message message) {
        amazonSQS.deleteMessage(queue, message.getReceiptHandle());
        log.info("drop illegal message, {}, {}", message.getBody(), message.getReceiptHandle());
    }
}
