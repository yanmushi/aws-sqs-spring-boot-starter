package com.yanmushi.aws.sqs;

import com.amazonaws.services.sqs.model.Message;

/**
 * @author yinlei
 * @since 2021/1/26
 */
public interface MessageObjectConverter<T extends SqsObject> {

    T message2Obj(Message message) throws IllegalMessageException;

    Message obj2Message(T t);

    void log(String action, SqsObject obj);

    boolean useAttr();
}
