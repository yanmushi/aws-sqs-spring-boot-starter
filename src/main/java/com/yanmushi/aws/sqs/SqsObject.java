package com.yanmushi.aws.sqs;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

/**
 * @author yinlei
 * @since 2021/1/26
 */
@Getter
@Setter
public abstract class SqsObject {

    @JsonIgnore
    private String sqsMessageId;
    @JsonIgnore
    private String sqsReceiptHandle;

}
