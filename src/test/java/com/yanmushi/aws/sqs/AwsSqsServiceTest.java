package com.yanmushi.aws.sqs;

import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.amazonaws.services.sqs.model.Message;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author yinlei
 * @since 2021/1/26
 */
public class AwsSqsServiceTest {

    private static final Logger log = LoggerFactory.getLogger(MockConverter.class);
    AwsSqsService awsSqsService;

    public static class MockObject extends SqsObject {
        String uuid;

    }
    public static class MockConverter implements MessageObjectConverter<MockObject> {

        @Override
        public MockObject message2Obj(Message message) throws IllegalMessageException {
            MockObject obj = new MockObject();
            obj.uuid = message.getBody();

            if ("illegal".equals(obj.uuid)) {
                throw new IllegalMessageException("illegal message : " + obj.uuid);
            }

            return obj;
        }

        @Override
        public Message obj2Message(MockObject s) {
            Message message = new Message().withBody(s.uuid);
            return message;
        }

        @Override
        public void log(String action, SqsObject obj) {
            MockObject mockObj = (MockObject) obj;
            log.info("{} message: {}", action, mockObj.uuid);
        }

        @Override
        public boolean useAttr() {
            return false;
        }
    }


    @Before
    public void setup() throws Exception {
        AmazonSQS amazonSQS = AmazonSQSClientBuilder.standard()
                .withCredentials(new ProfileCredentialsProvider("your profile"))
                .build();
        awsSqsService = new AwsSqsService();
        awsSqsService.setAmazonSQS(amazonSQS);
        awsSqsService.setQueueName("queue name");
        awsSqsService.setTimeout(5);
        awsSqsService.setMessageObjectConverter(new MockConverter());
        awsSqsService.setUpdateVisibilityInterval(1);

        awsSqsService.afterPropertiesSet();
    }

    @Test
    public void clean() {
        awsSqsService.ack(awsSqsService.receive());

    }

    @Test
    public void send() throws InterruptedException {
        MockObject obj = new MockObject();
        obj.uuid = "test123";
        awsSqsService.send(obj);


        SqsObject msg = awsSqsService.receive();
        tryGet();
        tryGet();

        Thread.sleep(6000);
        awsSqsService.ack(msg);

        Thread.sleep(6000);

    }

    @Test
    public void illegalMessage() {
        MockObject obj = new MockObject();
        obj.uuid = "illegal";

        awsSqsService.send(obj);

        SqsObject msg = awsSqsService.receive();

        log.info("get message, {}", msg);
    }

    private void tryGet() throws InterruptedException {
        Thread.sleep(6000);
        SqsObject msg = awsSqsService.receive();

        log.info("get msg: {}", msg);
    }
}