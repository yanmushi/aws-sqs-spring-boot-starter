# aws-sqs-spring-boot-starter

#### 介绍
在spring-boot项目中，引用AWS SQS服务，封装了消息的发送、接受、ACK操作，并支持消息可见性自动延长场景。更便于基于SQS的业务功能开发。

#### 项目依赖

```xml

<dependency>
    <groupId>com.yanmushi</groupId>
    <artifactId>aws-sqs-spring-boot-starter</artifactId>
    <version>1.0.0-SNAPSHOT</version>
</dependency>
```

#### 属性配置

AwsSqsService初始化示例

```java
    @Bean
    public AwsSqsService awsSqsService() {
        AwsSqsService service = new AwsSqsService();

        service.setAmazonSQS(amazonSQS);
        service.setQueueName("queue name");
        service.setMessageObjectConverter("your custom converter");
        service.setTimeout(timeout);
        service.setUpdateVisibilityInterval(interval);

        return service;
    }
```

属性说明

属性 | 是否必须 | 说明
--- | --- | ---
AmazonSQS | 是 | 消息队列客户端
QueueName | 是 | 消息队列名称
MessageObjectConverter | 是 | 消息与实体对象之间转换器，需要自定义实现
Timeout | 否 | 默认：60s。消息不可见时间。
UpdateVisibilityInterval | 否 | 默认：5s。到期5s前，延长消息不可见时间
IllegalMessageHandler | 否 | 默认：丢弃策略。可自定义扩展

#### 运行测试

参考测试类：AwsSqsServiceTest